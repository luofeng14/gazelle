%define conf_path %{_sysconfdir}/%{name}

Name:          gazelle
Version:       1.0.2
Release:       3
Summary:       gazelle is a high performance user-mode stack
License:       MulanPSL-2.0
URL:           https://gitee.com/openeuler/gazelle
Source0:       %{name}-%{version}.tar.gz

BuildRequires: cmake gcc-c++
BuildRequires: lwip >= 2.1.3-75
BuildRequires: dpdk-devel >= 21.11-5
BuildRequires: numactl-devel libpcap-devel libconfig-devel libboundscheck uthash-devel

Requires:      dpdk >= 21.11-5
Requires:      numactl libpcap libconfig libboundscheck

Patch9001:     0001-remove-unused-dpdk-dynamic-libraries.patch
Patch9002:     0002-gazelle_stat_pkts-gazelle_stat_low_power_info.patch
Patch9003:     0003-set-localhost-ip-when-bind-ip-is-same-as-ip-in-lstac.patch
Patch9004:     0004-refactoring-preload-module.patch
Patch9005:     0005-fix-previous-commit-refactoring-preload-module.patch
Patch9006:     0006-add-lstack_preload.c-to-makefile.patch

%description
%{name} is a high performance user-mode stack.

ExclusiveArch: x86_64 aarch64

%prep
%autosetup -n %{name}-%{version} -p1

%build
#export DPDK_VERSION_1911=1
cd %{_builddir}/%{name}-%{version}
# Add compile option, ignore map address check. Scenarios: asan test
%if 0%{?gazelle_map_addr_nocheck}
    sed -i 's/-pthread/-pthread -D gazelle_map_addr_nocheck/' %{_builddir}/%{name}-%{version}/src/ltran/CMakeLists.txt
%endif
sh build/build.sh

%install
install -dpm 0755 %{buildroot}/%{_bindir}
install -dpm 0755 %{buildroot}/%{_prefix}/lib64
install -dpm 0750 %{buildroot}/%{conf_path}

install -Dpm 0500 %{_builddir}/%{name}-%{version}/src/lstack/liblstack.*     %{buildroot}/%{_libdir}/
install -Dpm 0640 %{_builddir}/%{name}-%{version}/src/lstack/lstack.Makefile %{buildroot}/%{conf_path}/
install -Dpm 0640 %{_builddir}/%{name}-%{version}/src/lstack/lstack.conf     %{buildroot}/%{conf_path}/

install -Dpm 0500 %{_builddir}/%{name}-%{version}/src/ltran/gazellectl       %{buildroot}/%{_bindir}/
install -Dpm 0500 %{_builddir}/%{name}-%{version}/src/ltran/ltran            %{buildroot}/%{_bindir}/
install -Dpm 0640 %{_builddir}/%{name}-%{version}/src/ltran/ltran.conf       %{buildroot}/%{conf_path}/

%files
%defattr(-,root,root)
%dir %{conf_path}
%{_bindir}/*
%{_libdir}/liblstack.*
%{conf_path}/lstack.Makefile
%config(noreplace) %{conf_path}/lstack.conf
%config(noreplace) %{conf_path}/ltran.conf

%changelog
* Sat Aug 19 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.2-3
- add lstack_preload.c to makefile fix compile failure
- fix previous commit "refactoring preload module"
- refactoring preload module
- set localhost ip when bind ip is same as ip in lstack.conf

* Sat Jul 8 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.2-2
- sync remove unused dpdk dynamic libraries

* Tue Jul 4 2023 jiangheng12 <chengyechun1@huawei.com> - 1.0.2-1
- update version to 1.0.2

* Mon Jul 3 2023 jiangheng12 <chengyechun1@huawei.com> - 1.0.1-72
- bond4 add dpdk return value check

* Sat Jul 1 2023 jiangheng12 <chengyechun1@huawei.com> - 1.0.1-71
- fix bond4 EPOLLOUT event error

* Fri Jun 30 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-70
- add build requires constraints on lwip version

* Tue Jun 27 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-69
- add udp_enable to turn off udp in need

* Wed Jun 21 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-68
- check return value of hugepage_init to avoid coredump when hugepage memory is insufficient
- fix core dump when slave mac failed

* Sun Jun 25 2023 kircher <majun65@huawei.com> - 1.0.1-67
- enable UDP CKSUM in gazelle
- lstack: cfg add app_exclude_cpus
- skip gro when tcp/ip checksum offloads disable
- fix null pointer of sock in udp_recvfrom

* Mon Jun 19 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-66
- add multicast enable in dpdk_ethdev_init
- add use_sockmap in cfg to distinguish whether to use dpdk ring communication between processes

* Mon Jun 19 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-65
- remove obsolete args num_wakeup in doc

* Thu Jun 15 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-64
- change send_ring_size 32 in lstack conf
- adapt to dpdk-19.11 and dpdk-21.11
- fix t_params use after free in kernel event thread
- rpc pool use dpdk mempool replace array

* Wed Jun 14 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-63
- set sock when select path is PATH_UNKNOW

* Wed Jun 14 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-62
- fix udp send/recv in muliple queue
- fix gazellectl block before lstack registration is complete
- add exception handling for is_dst_ip_localhost
- fix change low power mod invalid
- send/recv thread bind numa only app_bind_numa set to 1

* Tue Jun 6 2023 jiangheng <jiangheng14@huawei.com> - 1.0.1-61
- revert cleancode series patches

* Fri Jun 2 2023 LemmyHuang <huangliming5@huawei.com> - 1.0.1-60
- fix socket init and clean
- fix coredump caused by lwip_get_socket_nouse

* Mon May 29 2023 LemmyHuang <huangliming5@huawei.com> - 1.0.1-59
- drop netbuf in read_lwip_data to fix mem overflow
- cleancode: refactor memp
- cleancode: refactor sys_now and lwip_ioctl
- cleancode: refactor gazelle_hlist.h
- cleancode: refactor gazelle_list.h
- cleancode: refactor gazelle_posix_api.h
- cleancode: refactor lwipsock.h
- cleancode: rename gazelle files in lwip
- cleancode: improving makefile readability

* Sat May 20 2023 jiangheng <jiangheng14@huawei.com> - 1.0.1-58
- modify hugepage directory name

* Tue May 16 2023 kircher <majun65@huawei.com> - 1.0.1-57
- add udp multicast support in gazelle
- clean code
- add bond4 suport
- dfx: security function failed, return error directly
- dfx: set g_unix_fd to -1 after abnormal close fd to avoid double close
- fix bond_ports parse error fix socket_mem parse error when the value exceeds 65536 exit if create_rxtx_pktmbuf failed build.sh build failed return 1 clean code
- add parentheses to fix build error
- fix rpc msg alloc failed fix process_numa args error coredump fix sock->conn not free when fd is kernel mode
- kni down not stop nic
- fix client connect number unbalance on lstack
- fix gazellectl -x error when multiplt user nic config

* Sat May 13 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-56
- fix build err with dpdk-21.11
- fix config flow rule race
- update lstack.Makefile
- add socket check before write it
- optimize do_close
- set kni_switch valid only in primary process
- fix build err on select_path
- optimite select_path and pbuf_take
- check primary process idx and secondary lstack num
- revert select_thread_path and optimize app thread when sendmsg
- do not transfer broadcast arp pkts to other process

* Thu Apr 20 2023 sunsuwan <sunsuwan3@huawei.com> - 1.0.1-55
- sepeate_string_to array add error args handle

* Thu Apr 20 2023 wu-changsheng <wuchangsheng2@huawei.com> - 1.0.1-54
- waiting when primary process not start already
- gazelle send/recv thread bind numa

* Tue Mar 21 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-53
- fix parse args error
- discard wakeup_num parameter
- fix kernel scoket select path error

* Sat Mar 18 2023 jiangheng<jiangheng14@huawei.com> - 1.0.1-52
- add pbuf lock when aggregate pbuf
- support multi process
- add gazellectl -x -a args
- add same node ring for iner-process communication
- fix send reset by peer when not sleep after connect
- add tuple filter in conf to diff rss rule adn tuple filter rule
- support tuple rule add/delete
- disable tso without ipv4 checksum
- refactor mbuf private data

* Sat Mar 11 2023 kircher <majun65@huawei.com> - 1.0.1-51
- when send ring full whether dynamic alloc mbuf is configurable reduce cpu usage when send ring full
- send should return -1, errno EAGAIN when ring full
- send ring size is configure
- remove rxtx driver cache
- fix do_close core dump
- fix private data offset error

* Mon Feb 27 2023 wu-changsheng <wuchangsheng2@huawei.com> - 1.0.1-50
- reduce duplicate code in lstack_cfg.c
- adapt unsupport sock optname

* Wed Feb 22 2023 kircher <majun65@huawei.com> - 1.0.1-49
- eneble TSO and fix TSO mbuf pktlen error
- check and fix wakeup_list when null appears

* Mon Feb 13 2023 net <jiangheng14@huawei.com> - 1.0.1-48
- change mbuf_pool_size in lstack.conf to tcp_conn_count * mbuf_count_per_conn
- bring up kni when init
- fix coredump in example server mum mode
- add fucntest
- test readv writev epoll_create1 accept4

* Mon Feb 6 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-47
- add gazelle setup tools
- add unitest

* Tue Jan 31 2023 kircher <majun65@huawei.com> - 1.0.1-46
- add gazelle fuzz
- add log message when wait for connecting to ltran

* Mon Jan 16 2023 kircher <majun65@huawei.com> - 1.0.1-45
- add ret check in pthread_create and fix example bug
- move control_client_thread creation after control_in and dpdk_skip_nic_init

* Fri Jan 6 2023 kircher <majun65@huawei.com> - 1.0.1-44
- add the suggestion of using the -u parameter when the connection to unix socket fails

* Fri Dec 30 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-43
- revert expand recv data buff

* Wed Dec 28 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-42
- pbuf cacheline align
  support main thread affinity
  reduce epoll wakeup

* Fri Dec 23 2022 kircher <majun65@huawei.com> - 1.0.1-41
- fix null pointer deref in stack_broadcast_close
- fix lstack Makefile warning

* Thu Dec 22 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-40
- add dfx rcv_nxt info
  mbuf private cache line align
  send pkts index bug fix
  free recv pkts in main loop

* Wed Dec 21 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-39
- add mempool dfx info
  write support without epoll/poll

* Tue Dec 20 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-38
- optimite recv data buff and send pkts index

* Sun Dec 18 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-37
- pkts-bulk-send-to-nic and rpc-dont-send

* Sat Dec 17 2022 jiangheng <jiangheng14@huawei.com> - 1.0.1-36
- remove mbuf reserve in mbuf alloc

* Sat Dec 17 2022 jiangheng <jiangheng14@huawei.com> - 1.0.1-35
- optimite net type
  app bind numa when epoll/poll create
  stack thread params set dafult value

* Sat Dec 17 2022 kircher <majun65@huawei.com> - 1.0.1-34
- add RXTX_NB_MBUF_MAX to limit mbuf_pool_size to its range

* Fri Dec 16 2022 kircher <majun65@huawei.com> - 1.0.1-33
- move select_thread_path after posix_api_init

* Thu Dec 15 2022 jiangheng <jiangheng14@huawei.com> - 1.0.1-32
- ltran rxtx mbuf pool size config by conf

* Thu Dec 15 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-31
- stack thread params config by lstack.conf

* Wed Dec 14 2022 jiangheng <jiangheng14@huawei.com> - 1.0.1-30
- fix kernel event thread bind nuam failed

* Tue Dec 13 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-29
- dfx add pcb windows info
  rxtx mbuf pool size config by conf

* Mon Dec 12 2022 kircher <majun65@huawei.com> - 1.0.1-28
- add pdump support in ltran

* Sat Dec 3 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-27
- optimize app thread write buff block

* Fri Dec 2 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-26
- fix epoll_wait report events 0

* Thu Dec 1 UTC compile_success <980965867@qq.com> - 1.0.1-25
- add malloc init zero
- modify EPOLLOUT event is overwritten.

* Sat Nov 26 UTC compile_success <980965867@qq.com> - 1.0.1-24
- fix write event error

* Wed Nov 16 2022 kircher <majun65@huawei.com> - 1.0.1-23
- modify duplicate code
- fix data flow error when use NIC in kernel
- fix lwip send return 0 add err event
- fix pcb snd_buf flip
- avoid send stop when mbuf pool empty
- merge lstack rx tx mbuf pool

* Tue Nov 15 2022 kircher <majun65@huawei.com> - 1.0.1-22
- fix pdump and mutil NIC init fail

* Mon Nov 14 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-21
- support muti-nic
  fix some bugs

* Tue Nov 8 2022 kircher <majun65@huawei.com> - 1.0.1-20
- add pdump support in lstack

* Sat Nov 07 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-19
- resolve the conflict between the eth_dev_ops variable and the dpdk-19.11

* Sat Nov 05 2022 kircher <majun65@huawei.com> - 1.0.1-18
- Add kni local support in lstack

* Fri Nov 04 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-17
- Optimize ceph client performance

* Sat Oct 08 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-16
- refactor event
  addapt for ceph client

* Mon Sep 05 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-15
- expand rpc msg pool size

* Mon Sep 05 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-14
- backport bugfix and doc

* Mon Aug 08 2022 fushanqing <fushanqing@kylinos.cn> - 1.0.1-13
- Unified license name specification

* Tue Jul 26 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-12
- support epoll oneshot 

* Tue Jul 19 2022 xiusailong <xiusailong@huawei.com> - 1.0.1-11
- reconstruct packet sending and receiving to improve performance 

* Thu Jul 7 2022 jiangheng <jiangheng14@huawei.com> - 1.0.1-10
- Type:bugfix
- CVE:
- SUG:NA
- DESC:update readme
       fix some bugs
       refactor pkt read send to improve performance
       refactoe kernle event to improve performanc 

* Fri May 27 2022 xiusailong <xiusailong@huawei.com> - 1.0.1-9
- update license lockless queue

* Fri May 20 2022 xiusailong <xiusailong@huawei.com> - 1.0.1-8
- update README.md

* Thu Mar 31 2022 jiangheng <jiangheng12@huawei.com> - 1.0.1-7
- Type:bugfix
- CVE:
- SUG:NA
- DESC:add gazelle.yaml

* Tue Mar 29 2022 jiangheng <jiangheng12@huawei.com> - 1.0.1-6
- refactor event
- add gazellectl lstack constraint

* Fri Mar 18 2022 jiangheng <jiangheng12@huawei.com> - 1.0.1-5
- limit lwip_alloc_pbuf size to TCP_MSS
- sending of sock last data is triggered by lstack iteself 

* Thu Mar 17 2022 jiangheng <jiangheng12@huawei.com> - 1.0.1-4
- fix repeatede stack restart coredump

* Wed Mar 16 2022 jiangheng <jiangheng12@huawei.com> - 1.0.1-3
- fix gazelle test issue

* Mon Mar 7 2022 wu-changsheng <wuchangsheng2@huawei.com> - 1.0.1-2
- reduce copy in send

* Thu Mar 3 2022 wu-changsheng <wuchangsheng2@huawei.com> - 1.0.1-1
- support mysql with two mode:ltran+lstack and lstack.

* Thu Feb 24 2022 wu-changsheng <wuchangsheng2@huawei.com> - 1.0.0-1
- release initial version
